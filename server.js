const http = require('http');

const app = require('./app'); //

const port = process.env.port || 8080; 

 const server = http.createServer(app); // pass a listener (a function) to return a response

server.listen(port); // starts listening to the port and then executes the above function call

