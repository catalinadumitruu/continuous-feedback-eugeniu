Continuous Feedback - Tema 6

Membri: -> Georgiana-Catalina Dumitru(TeamLeader)
        -> Georgiana-Iulia Nila(Delivery Manager)
        -> Georgiana Neacsu(Software Engineer)
        -> Laurentiu-Andrei Oprea(Arhitect)


Roluri:

-> TeamLeader:Georgiana-Catalina Dumitru ->Coordoneaza activitatile de dezvoltare legate de partea de frontend si backend ale aplicatiei Continuous feedback pentru a putea fi in conformitate cu cerintele clientului.
                                        ->Raspunde de nivelul etic si profesional al echipei pe care o supervizeaza.
                                        ->Realizeaza si trimite periodic raportari.
                                        ->Este responsabil pentru procesul de alocari.
                                        

-> Delivery Manager:Georgiana-Iulia Nila->Intelege strategia de dezvoltare,obiectivele si procesele de lucru ale clientilor.
                                        ->Se asigura de livrarea proiectului in concordanta cu acesta.
                                        ->Stabileste abordari si strategii de urmat pentru fiecare angajat.
                                        ->Planifica executia si monitorizeaza statusul si progesul proiectului.

->Software Engineer:Georgiana Neacsu->Efectueaza analiza cerintelor.
                                    ->Creeaza si revizuieste specificatiile software-ului.
                                    ->Dezvoltator si asistenta(parte de backend).
                                    
                                    
->Arhitect:Laurentiu-Andrei Oprea->Proiecteaza interfata aplicatiei(parte de frontend).

*Toti membri echipei au contribuit la dezvoltarea aplicatiei din punct de vedere frontend si backend;


Specificatii (finale - ToBe):

Aplicatie web formata din parte de frontend si backend, utilizand HTML, CSS si Javascript si un RESTful API.
Aplicatia va avea la baza fisiere JSON, in care se stocheaza informatia.


Descriere:

1.Ca profesor pot defini o activitate la o anumită dată, cu o descriere și un cod unic de acces la activitate. 
Activitatea poate fi accesată pentru o durată prestabilită de timp.

2.Ca student pot introduce un cod pentru a participa la o activitate definită. Codul este valabil pentru durata activității

3.Ca student care a accesat o activitate am acces la o interfața împarțită în 4 cadrane fiecare cu un emoticon (smiley face, frowny face, surprised face, confused face). 
În orice moment pot apăsa un emoticon pentru a reacționa la activate. Ca student pot să adaug oricâte instanțe de feedback.

4.Ca profesor pot vede feedback-ul continuu cu momentele de timp asociate. Feedback-ul este anonim. Feedback-ul este accesibil atât în timpul activității cât și ulterior


Instructiuni de folosire:

