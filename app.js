// makes handly requests
'use strict'
require('dotenv').config({path:'./.env'}, {silence:true});
//console.log("*******" + process.env.DB_USER)

const express = require('express');
const app = express();  
const morgan = require('morgan');
const bodyParser = require('body-parser');

const cors = require('cors')

//const studentRoutes = require('./api/routes/student-router');
//const teacherRoutes = require('./api/routes/teacher-router');

const router = require('./api/routers');

// app.use((req, res, next) =>  { //sets up a middleware , with diff formats
//         res.status(200).json({  //send a JSON reponse with status EVERYTHING OK
//             message : "BUNA, WE ARE THE GEORGIANS"
//         });
// });

app.use(cors())
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static('./frontend-continuous-feedback/build')); //pentru lucru cu fisiere, deserializare/serializare
app.use(bodyParser.json()); // makes text easier to read
 // true allows to parse extended body with complex date 


 // prevent CORS Errors
app.use((req, res, next) =>{
    res.header('Access-Control-Allow-Origin', '*'); //gives access to all
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization'); //what headers are accepted

    if(req.method === 'OPTIONS'){ //the browser sees if you are allowed to do a request
        res.header('Access-Control-Allow-Origin', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }

    next(); // if we are not returning anything put this so the other routes can take over
});

app.use('/students', router.student);
app.use('/teachers', router.teacher);
app.use('/activities', router.activity);
app.use('/feedbacks', router.feedback);

//catches all errors
app.use(( req, res, next) =>{
    const error = new Error('Not found');  //errors handler
    error.status = 404;
    next(error);  // forward the custom error
});

app.use((error, req, res, next) =>{
    res.status(error.status || 500);
    res.json({
        error:{
            message : error.message
        }
    })
});


module.exports = app;