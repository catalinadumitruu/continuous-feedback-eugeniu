//comunicare cu serverul
import {EventEmitter} from 'fbemitter'

const SERVER = "http://localhost:8080"

class ActivityStore{
    constructor(){
        this.activities = []
        this.emitter=new EventEmitter();
    }

    //request la server
    async getAll(){
        try{
            let response = await fetch(`${SERVER}/activities`)
            let data = await response.json();
            this.activities = data;
            this.emitter.emit('GET_ACTIVITIES_SUCCESS')
        }catch(err){
            console.log(err)
            this.emitter.emit('GET_ACTIVITIES_FAILED')

        }
    }

    async getTime(url){
        try{
            let response = await fetch(url)
            let data = await response.json();
            this.activities = data;
            this.emitter.emit('GET_ACTIVITIES_SUCCESS')
        }catch(err){
            console.log(err)
            this.emitter.emit('GET_ACTIVITIES_FAILED')

        }
    }

    async addOne(activity){
        try{
            await fetch(`${SERVER}/activities`, {
                method:'post',
                headers:{
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(activity)
            })
            
           this.getAll()
        }catch(err){
            console.log(err)
            this.emitter.emit('POST_ACTIVITIES_FAILED')
        }
    }
}
 
export default ActivityStore