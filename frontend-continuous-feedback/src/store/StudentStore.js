//comunicare cu serverul
import {EventEmitter} from 'fbemitter'

const SERVER = "http://localhost:8080"

class StudentStore{
    constructor(){
        this.students = []
        this.emitter=new EventEmitter();
    }

    //request la server
    async getAll(){
        try{
            let response = await fetch(`${SERVER}/students`)
            let data = await response.json();
            this.students = data;
            this.emitter.emit('GET_STUDENTS_SUCCESS')
        }catch(err){
            console.log(err)
            this.emitter.emit('GET_STUDENTS_FAILED')

        }
    }

    // async get(student){
// si la teacher
    // }

    async addOne(student){
        try{
            await fetch(`${SERVER}/students`, {
                method:'post',
                headers:{
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(student)
            })
            
           this.getAll()
        }catch(err){
            console.log(err)
            this.emitter.emit('POST_STUDENT_FAILED')
        }
    }
}
 
export default StudentStore