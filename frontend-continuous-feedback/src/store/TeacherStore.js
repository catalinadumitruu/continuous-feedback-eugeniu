//comunicare cu serverul
import {EventEmitter} from 'fbemitter'

const SERVER = "http://localhost:8080"

class TeacherStore{
    constructor(){
        this.teachers = []
        this.emitter=new EventEmitter();
    }

    //request la server
    async getAll(){
        try{
            let response = await fetch(`${SERVER}/teachers`)
            let data = await response.json();
            this.teachers = data;
            this.emitter.emit('GET_TEACHERS_SUCCESS')
        }catch(err){
            console.log(err)
            this.emitter.emit('GET_TEACHERS_FAILED')

        }
    }

    async addOne(teacher){
        try{
            await fetch(`${SERVER}/teachers`, {
                method:'post',
                headers:{
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(teacher)
            })
            
           this.getAll()
        }catch(err){
            console.log(err)
            this.emitter.emit('POST_TEACHER_FAILED')
        }
    }
}
 
export default TeacherStore