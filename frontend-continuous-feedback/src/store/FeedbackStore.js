//comunicare cu serverul
import {EventEmitter} from 'fbemitter'

const SERVER = "http://localhost:8080"

class FeedbackStore{
    constructor(){
        this.feedbacks = []
        this.emitter=new EventEmitter();
    }

    //request la server
    async getAll(){
        try{
            let response = await fetch(`${SERVER}/feedbacks`)
            let data = await response.json();
            this.feedbacks = data;
            this.emitter.emit('GET_FEEDBACKS_SUCCESS')
        }catch(err){
            console.log(err)
            this.emitter.emit('GET_FEEDBACKS_FAILED')

        }
    }

    async addOne(feedback){
        try{
            await fetch(`${SERVER}/feedbacks`, {
                method:'post',
                headers:{
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(feedback)
            })
            
           this.getAll()
        }catch(err){
            console.log(err)
            this.emitter.emit('POST_FEEDBACKS_FAILED')
        }
    }
}
 
export default FeedbackStore