import React from 'react';
import {Component} from 'react'; // {} importam o cheie
import StudentList from './component/StudentList';
import Menu from './component/Menu.js'
import {Route,Switch } from 'react-router-dom';
import StudentLogin from './component/studentLogin';
import TeacherLogin from './component/teacherLogin';
import Citat from './component/citat';
import About from './component/about';
import Feedback from "./component/enterActivity";
import ProfilTeacher from "./component/profilTeacher";
import ActivityForm from "./component/ActivityForm";
import ViewActivity from './component/viewActivity';
import Teacher_login_register from './component/teacher_login_or_register'
import TeacherRegister from './component/teacher_register'
import FeedbackView from './component/viewFeedbacks'
import './App.css';


class App extends Component{
  constructor(){
    super();
    console.log("app is constructed")
    this.somevar= "some var"
  }

  componentDidMount(){  //atasez la DOM
    console.log("app is mounted")
  }

  componentDidUpdate(){
    console.log("app is updated");
  }

  render(){

    let links = [
      {label:'Home', link:'#home', active:true},
      {label:'Student Login', link:'#studLogin'},
      {label:'Teacher Login', link:'#teacherLogin'},      
      {label:'About', link:'#about'},
    ];

    return (
    //   <div className="container center">
    //     <Menu links={links}/>
    // </div>
    <div className="principal">
      <div className="container center">
        
        <Menu links={links}/> 
         

      </div>
      <div className="continut-pag">
      <Switch>
          
          <Route path="/studentLogin" component={StudentLogin}/>
          <Route path="/teacherLogin" component={TeacherLogin}/>
          <Route path="/about" component={About}/>
          <Route path="/feedback" component={Feedback}/>
          <Route path="/profilTeacher" component={ProfilTeacher}/>
          <Route path="/addActivity" component={ActivityForm}/>
          <Route path="/viewActivity" component={ViewActivity}/>
          <Route path="/teacher_login_register" component={Teacher_login_register}/>
          <Route path="/teacherRegister" component={TeacherRegister}/>
          <Route path="/viewFeedbacks" component={FeedbackView}/>
          <Route path="" component={Citat}/>
          
      </Switch> 
      </div>
    
    </div>
    );
  }
}

export default App;
