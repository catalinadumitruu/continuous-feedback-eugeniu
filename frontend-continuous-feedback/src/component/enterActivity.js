import  React,{ Component } from 'react'
import './enterActivity.css'
import smiley from './1.png'
import sad from './2.png'
import love from './3.png'
import angry from './4.png'
import axios from 'axios';

class Feedback extends Component{
    constructor(props){
        super(props);
        this.state = {
            feedbacks : []
           
        }
        this.onSubmit = this.onSubmit.bind(this); // punte de legatura intre render si onSubmit
        this.onSubmit2 = this.onSubmit2.bind(this);
        this.onSubmit3 = this.onSubmit3.bind(this);
        this.onSubmit4 = this.onSubmit4.bind(this);
        

        //handle de eveniment
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }

    addFeedback(activity){
      axios.request({
          method: 'post',
          url: 'http://localhost:8080/feedbacks',
          data: activity
      })
      .then(response =>{
          this.props.history.push('/studentLogin');
      })
      .catch(err => console.log(err));
  }

  currentTime(){
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date + ' ' + time;
    return dateTime;
  }

    onSubmit(event) {
      event.preventDefault();
      const newFeedback = {
        description: this.refs.smiley.alt,
        time: this.currentTime(),
        idActivity : this.refs.receivedID.value
    }

      console.log(newFeedback);
      this.addFeedback(newFeedback);
    }

    onSubmit2(event) {
        event.preventDefault();
        const newFeedback = {
          description: this.refs.sad.alt,
          time: this.currentTime(),
          idActivity : this.refs.receivedID.value
      }

        console.log(newFeedback);
        this.addFeedback(newFeedback);
      }

      onSubmit3(event) {
        event.preventDefault();
        const newFeedback = {
          description: this.refs.love.alt,
          time: this.currentTime(),
          idActivity : this.refs.receivedID.value
      }

        console.log(newFeedback);
        this.addFeedback(newFeedback);
      }

      onSubmit4(event) {
        event.preventDefault();
        const newFeedback = {
          description: this.refs.angry.alt,
          time: this.currentTime(),
          idActivity : this.refs.receivedID.value
      }
        console.log(newFeedback);
        this.addFeedback(newFeedback);
      }

    render(){
      var receivedID = this.props.history.location.data
        return(
            <div className="principalLog" >

                <label for="id_of_activ" id="stat">Give feedback for activity with ID   </label>
                <input id="id_of_activ" disabled ref="receivedID" value={receivedID} />

                <div className="stanga">
                    <div className="sidebar-item"><img className="images" ref="smiley" src={smiley} alt="smiley-face" onClick={this.onSubmit}></img></div>
                    <br/>
                    <div className="sidebar-item"><img className="images" ref="sad" src={sad} alt= "sad-face"onClick={this.onSubmit2}></img></div> 
                </div>
                <div className="dreapta">
                    <div className="sidebar-item"><img className="images" ref="love" src={love} alt="love"onClick={this.onSubmit3}></img></div>
                    <br/>
                    <div className="sidebar-item"><img className="images" ref="angry" src={angry} alt="angry-face"onClick={this.onSubmit4}></img></div>
                </div>
            </div>
        
        );
    }
}

const style = {margin:15};

export default Feedback