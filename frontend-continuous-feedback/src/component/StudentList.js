import React from 'react';
import {Component} from 'react'; // {} importam o cheie
import StudentStore from '../store/StudentStore'
import StudentForm from './StudentForm';

class StudentList extends Component{
  constructor(){
    super();
    this.state={
      students : []
    }
    this.store = new StudentStore();

    this.add = (student) => {
      this.store.addOne(student);
    }
  }

  componentDidMount(){
    this.store.getAll()
    this.store.emitter.addListener('GET_STUDENTS_SUCCESS', () => {
      //daca s-a intamplat cu succes
      this.setState({
        students: this.store.students
      })
    })
  }

  //dupa ce am salvat datele din baza in state.students
  render(){
    return <div> 
       {
         this.state.students.map((e,i) => <div key={i}>
                <h1>
                  {e.name}  {e.surname}
                </h1>
                <h2>
                  {e.year} {e.class}
                </h2>
           </div>
         )
       }

       <StudentForm onAdd={this.add}/>
    </div>
  }
}

export default StudentList;
