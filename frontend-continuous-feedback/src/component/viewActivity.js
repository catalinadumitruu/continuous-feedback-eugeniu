import React, { Component } from 'react'
import axios from 'axios';
import {ToastsContainer, ToastsStore} from 'react-toasts';
import './viewActivity.css'

class ViewActivity extends Component {

    constructor(props) {
        super(props);

        this.state = {
            activityID: []
        }

        this.onSubmit = this.onSubmit.bind(this)

        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name]: evt.target.value
            })
        }
    }

    async searchActivity(activityID) {
        await axios.request({
            method: 'GET',
            url: 'http://localhost:8080/activities/' + activityID
        }).then((response) => {
            this.setState({ activityID: response.data })
        })
    }

    async onSubmit(event) {
        event.preventDefault();
        const newActivityID = {
            activityID: this.refs.keyAccess.value
        }

        await this.searchActivity(newActivityID.activityID);

        if(this.state.activityID.length >= 1){  // daca exista cel putin o activitate..ar trebuie sa fie === 1 dar merge si asa
            this.props.history.push({
               pathname : '/viewFeedbacks',
               data : newActivityID.activityID
            });
        }else{
            ToastsStore.error("The inserted ID does not exist in database");
        }
    }

    render() {
        return (
            <div className="fields">
                <form id="viewActvity_form" onSubmit={this.onSubmit}>
                    <label for="keyAccess">Key Access      </label>
                    <input type='text' id='keyAccess' placeholder="Enter key access" ref="keyAccess" />
                    <br/>
                    <button id="buttonViewFeedbacks">View feedbacks</button>
                      <ToastsContainer store={ToastsStore}/>
                </form>
            </div>
        );
    }
}

const style = { margin: 15 };

export default ViewActivity