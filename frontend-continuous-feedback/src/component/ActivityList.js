import React from 'react';
import {Component} from 'react'; // {} importam o cheie
import ActivityStore from '../store/ActivityStore'
import ActivityForm from './ActivityForm';

class ActivityList extends Component{
  constructor(){
    super();
    this.state={
      activities : []
    }
    this.store = new ActivityStore();

    this.add = (activity) => {
      this.store.addOne(activity);
    }
  }

  componentDidMount(){
    this.store.getAll()
    this.store.emitter.addListener('GET_ACTIVITIES_SUCCESS', () => {
      //daca s-a intamplat cu succes
      this.setState({
        activities: this.store.activities
      })
    })
  }

  //dupa ce am salvat datele din baza in state.activities
  render(){
    return <div> 
       {
         this.state.activities.map((e,i) => <div key={i}>
                <h1>
                  {e.description}  {e.keyAccess}
                </h1>
           </div>
         )
       }
    </div>
  }
}

export default ActivityList;
