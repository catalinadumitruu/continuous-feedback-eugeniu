import React, { Component } from 'react'
import FeedbackStore from '../store/FeedbackStore';
import axios from 'axios';
import {ToastsContainer, ToastsStore} from 'react-toasts';
import './viewFeedbacks.css'

class viewFeedback extends Component {
  constructor() {
    super();
    this.state = {
      feedbacks: []
    }

    this.store = new FeedbackStore();

    this.onSubmit = this.onSubmit.bind(this); // punte de legatura intre render si onSubmit

    //handle de eveniment
    this.handleChange = (evt) => {
        this.setState({
            [evt.target.name] : evt.target.value
        })
    }
  }

  //la inceput afisam toate feedback-urile, intre timp am schimbat
  componentDidMount() {
    // this.store.getAll()
    // this.store.emitter.addListener('GET_FEEDBACKS_SUCCESS', () => {
    //   //daca s-a intamplat cu succes
    //   this.setState({
    //     feedbacks: this.store.feedbacks
    //   })
    // })
  }

  async searchFeedbacks(activityID){
    await axios.request({
        method: 'GET',
        url: 'http://localhost:8080/feedbacks/' + activityID
    }).then( (response) =>  {
         this.setState({feedbacks : response.data })
         console.log("in get " + JSON.stringify(this.state))
    });
}

async onSubmit(event) {
  event.preventDefault();
  const newActivity = {
      activity: this.refs.receivedID.value
  }

  await this.searchFeedbacks(newActivity.activity);
  console.log("in submit" + JSON.stringify(this.state))

  if(newActivity.activity == ''){
    ToastsStore.warning("There is no feedback for the given ID...showing all");
    console.log("Nu avem id");
  }
  else if(this.state.feedbacks.length < 1){
     ToastsStore.error('There are no feedbacks for this activity');
  }
}


  render() {
    var receivedID = this.props.history.location.data
    if (this.state.feedbacks != null) {
      return (
        <div className="listaFeedbacks">
          <div id="mesaj">
          <form onSubmit={this.onSubmit}>
          <p>Showing for activity with id : </p>
          <input disabled ref="receivedID" value={receivedID} />
          <button>OK</button>
          <ToastsContainer store={ToastsStore} />
          </form>
          </div>
          <div id ="tabel_feedbacks">
          <table>
            <tr>
              <td>Feedback</td>
              <td>Time</td>
            </tr>
            {this.state.feedbacks.map((e, i) =>
              <tr key={i}>
                <td>{e.description}</td>
                <td>{e.time}</td>
              </tr>)}
          </table>
          </div>
        </div>
      );
    } else {
      return <div>
        <h2>There aren't feedbacks for this activity in database at the moment</h2>
      </div>
    }
  }
}

export default viewFeedback;