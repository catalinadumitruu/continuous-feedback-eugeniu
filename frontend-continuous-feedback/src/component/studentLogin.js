import  React,{ Component } from 'react';
import './studentLogin.css'
import axios from 'axios';
import {ToastsContainer, ToastsStore} from 'react-toasts';
import ActivityStore from '../store/ActivityStore';

class StudentLogin extends Component{
    constructor(props){
        super(props);
        this.state = {
           activities : []
          
        }

        this.store = new ActivityStore();
        this.onSubmit = this.onSubmit.bind(this);

        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }


    async searchActivity(activityID){
        await axios.request({
            method: 'GET',
            url: 'http://localhost:8080/activities/' + activityID
        }).then( (response) =>  {
             this.setState({
                 activities : response.data
                }, ()=> {
                 console.log("search activity: " + JSON.stringify(this.state.activities[0]));
            });
        }).catch(err =>
            ToastsStore.error('This ID does not exists in database'));
    }

    currentTime(){
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;
        return dateTime;
      }

    // activityTime(activID){
    //     this.store.getTime('http://localhost:8080/activities/time/' + activID);
    //     this.store.emitter.addListener('GET_TIME_SUCCESS', () => {
    //         this.setState({
    //             activities : this.store.activities.startTime
    //         })
    //        // console.log(this.state.time)
    //     })
    // }

    async onSubmit(event) {
        event.preventDefault();
        const id_inserat = {
            activityID: this.refs.activityID.value
        }

        await this.searchActivity(id_inserat.activityID);
        console.log("search activity IN SUBMIT: " + JSON.stringify(this.state.activities[0]));
        console.log("activities in submit " + this.state.activities.length );
        console.log("in submit: " + this.state.activities[0]);

        var item=this.state.activities[0];
        
        console.log("acest item:"+ item.allowAcces);

        let currentTime = this.currentTime();
        console.log("Current time  " + currentTime);

        

        if(id_inserat.activityID == ''){
            ToastsStore.error('Please insert an ID');
        } 
        else if(this.state.activities.length >= 1){
            if(item.allowAcces=='NO'){
            ToastsStore.error('This activity is not available');

        }else{
            console.log("Avem cel putin o activitate cu id-ul asta");
            this.props.history.push({
               pathname: '/feedback',
               data : id_inserat.activityID
            });
        }
        }else{
            ToastsStore.error('There is no activity with the given ID');
        }
    }

    render(){
        return(
            <div id="divv">
                <form onSubmit={this.onSubmit} id="acestFrom">
                    <label for="AccessKey" id="label">Access Key    </label>
                    <input id="input" type='text' id='AccessKey' placeholder="Enter access key" ref="activityID"/>
                    <br/>
                    <div id="btn">
                        <br/>
                    <button id="butonLOGIN">Give feedback</button>
                        <ToastsContainer store={ToastsStore}/>
                        </div>
                </form>
            </div>
        );
    }
}

const style = {margin:15};

export default StudentLogin