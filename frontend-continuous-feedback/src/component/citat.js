import  React,{ Component } from 'react'
import './Citat.css'
import ActivityStore from '../store/ActivityStore';

class Citat extends Component{
    constructor(){
        super();
        this.state={
          activities : []
        }
        this.store = new ActivityStore();
      }

    componentDidMount(){
        this.store.getAll()
        this.store.emitter.addListener('GET_ACTIVITIES_SUCCESS', () => {
          //daca s-a intamplat cu succes
          this.setState({
            activities: this.store.activities
          })
        })
      }

    render(){
        if(this.state.activities != null){
            return(
                <div className="listaActivitati">
                <table>
                    <tr>
                        <td>Activity</td>
                        <td>Access key</td>
                    </tr>
                         {this.state.activities.map((e,i) =>  
                            <tr key={i}> 
                                <td>{e.description}</td>
                                <td>{e.keyAccess}</td>
                            </tr> )}
                 </table>
               </div>        
            );
        }else{
            return <div>
                <h2>There aren't activities in database at the moment</h2>
            </div>
        }
      
    }
}

export default Citat