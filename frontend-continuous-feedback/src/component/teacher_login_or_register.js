import  React,{ Component } from 'react'
import {Link} from 'react-router-dom'
import './teacher_login_or_register.css'

class teacher_login_or_register extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return <div>
            <Link className="login_or_reg" to="/teacherLogin" >LOGIN</Link>
            <Link className="login_or_reg" to="/teacherRegister" >REGISTER</Link>
        </div>
    }
}

const style = {margin:15};

export default teacher_login_or_register;