import React from 'react';
import {Component} from 'react'; // {} importam o cheie
import StudentList from '../component/StudentList';

class StudentForm extends Component{
    constructor(props){
        super(props);
        this.state = {
            surname: '',
            name: '',
            year: '',
            class: ''
        }

        //handle de eveniment
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }

  render(){
    return <div> 
        <input type='text' placeholder='surname' onChange={this.handleChange} name='surname'/>
        <input type='text' placeholder='name' onChange={this.handleChange} name='name'/>
        <input type='text' placeholder='year' onChange={this.handleChange} name='year'/>
        <input type='text' placeholder='class' onChange={this.handleChange} name='class'/>
        <input type='button' value='add' onClick={() => 
            this.props.onAdd({
                surname:this.state.surname,
                name:this.state.name,
                year:this.state.year,
                class:this.state.class
            })} name='content'/>
    </div>
  }
}

export default StudentForm;
