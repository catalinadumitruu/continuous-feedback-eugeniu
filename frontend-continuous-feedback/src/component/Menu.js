import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import './Menu.css'




class Menu extends Component{
    
    constructor(){
        super();

        this.state={
            showForm : false,
            studentLogin : false,
            teacherLogin : false,
            about : false
        };

    }

    showForm(){
        this.setState({
            showForm: !this.state.showForm
        });
    }

    home(){
        //show motivalional quote
        console.log('Home state ' + this.state.home);

    }
    
    studLog() {
        
        
        //  console.log('Student logged');
        //  console.log('Student state ' + this.state.studentLogin);

        // //login page for students

        //  this.setState({
        //      studentLogin: !this.state.studentLogin
        //  });
    }
    showQuote(){}
    teacherLog(){
        console.log("Teacher Logged")
        console.log('Teacher state ' + this.state.teacherLogin);
        
        //login page for teachers

        this.setState({
            teacherLogin: !this.state.teacherLogin
        });
    }

    about(){
        //read me of the project
        console.log('About state ' + this.state.about);

        this.setState({
            about: !this.state.about
        });
    }

    render(){
        let serachForm = this.state.showForm ? (
            <form className='menu__search-form' method='POST'>
                <input className='menu__search-input' placeholder='Type and hit enter'/>
            </form>
        ) : '';


        return(
        <nav className="menu">

            <h1 className="menu__logo">Student's page</h1>
           
            <div className="menu__right">
                <ul className="menu__list">
                    <li className='menu__list-item' ><Link to=""className='menu__link menu__link' >Home</Link></li>
                    <li className='menu__list-item' ><Link to="/studentLogin" className="menu__link menu__link" >Student Login</Link></li> 
                    <li className='menu__list-item'><Link to="/teacher_login_register" className="menu__link menu__link">Teacher Login</Link></li>
                    <li className='menu__list-item'><Link to="/about" className="menu__link menu__link">About</Link></li>
                </ul>

                <button className="menu__search-button" onClick={this.showForm.bind(this)}></button>
                {serachForm}
                
            </div>
            
            

        </nav>
       
        );
        
    }
}

export default Menu;