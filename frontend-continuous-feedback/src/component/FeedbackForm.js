import React from 'react';
import {Component} from 'react'; // {} importam o cheie
import axios from 'axios';

class FeedbackForm extends Component{
    constructor(props){
        super(props);
        this.state = {
            description: '',
            emoticon: '',
            time: '',
            idStudent: '',
            idActivity:''
        }
        this.onSubmit = this.onSubmit.bind(this); // punte de legatura intre render si onSubmit

        //handle de eveniment
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }

    addFeedback(activity){
      axios.request({
          method: 'post',
          url: 'http://localhost:8080/feedbacks',
          data: activity
      })
      .then(response =>{
          this.props.history.push('/');
      })
      .catch(err => console.log(err));
  }

    onSubmit(event) {
      event.preventDefault();
      const newFeedback = {
        description: this.refs.description.value,
        emoticon: this.refs.emoticon.value,
        time: this.refs.time.value,
        idStudent: this.refs.idStudent.value,
        idActivity:this.refs.idActivity.value
    }
      console.log("Test");
      this.addFeedback(newFeedback);
    }

  render(){
    return (
      <form onSubmit={this.onSubmit}>
          <h3>Add Product</h3>
          <input placeholder="description" ref="description"/>
          <br />
          <input placeholder="emoticon" ref="emoticon"/>
          <br />
          <input placeholder="time" ref="time"/>
          <br />
          <input placeholder="idStudent" ref="idStudent"/>
          <br />
          <input placeholder="idActivity" ref="idActivity"/>
          <br />
          <button>Add</button>
          
      </form>
  );
  }
}

export default FeedbackForm;
