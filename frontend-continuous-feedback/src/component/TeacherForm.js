import React from 'react';
import {Component} from 'react'; // {} importam o cheie
import axios from 'axios';


class TeacherForm extends Component{
    constructor(props){
        super(props);
        this.state = {
            teacherId: '',
            fullname: '' 
        }
        this.onSubmit = this.onSubmit.bind(this); // punte de legatura intre render si onSubmit

        //handle de eveniment
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }

    addTeacher(teacher){
      axios.request({
          method: 'post',
          url: 'http://localhost:8080/teachers',
          data: teacher
      })
      .then(response =>{
          this.props.history.push('/');
      })
      .catch(err => console.log(err));
  }

    onSubmit(event) {
      event.preventDefault();
      const newTeacher = {
        teacherId: this.refs.teacherId.value,
        fullname:this.refs.full.value
       
    }
      console.log("Test");
      this.addTeacher(newTeacher);
    }

  render(){
    return (
      <form onSubmit={this.onSubmit}>
          <h3>Add Teacher</h3>
          <input placeholder="teacherId" ref="teacherId"/>
          <br />
          <input placeholder="fullname" ref="fullname"/>
          
          <button>Add</button>
          
      </form>
  );
  }
}

export default TeacherForm;
