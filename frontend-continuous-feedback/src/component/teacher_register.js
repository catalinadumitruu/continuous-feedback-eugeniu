import  React,{ Component } from 'react'
import axios from 'axios';
import './teacher_register.css'
import {ToastsContainer, ToastsStore} from 'react-toasts';


class TeacherRegister extends Component{

    constructor(props){
        super(props);
        this.state = {
            teachers : []
        }
        this.onSubmit = this.onSubmit.bind(this); // punte de legatura intre render si onSubmit

        //handle de eveniment
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
    }
    
    addTeacher(teacher){
        axios.request({
            method: 'post',
            url: 'http://localhost:8080/teachers',
            data: teacher
        })
        .then(response =>{
            this.props.history.push('/profilTeacher');
        })
        .catch(err => console.log(err));
    }

  
    async searchTeacher(teacherID){
        await axios.request({
            method: 'GET',
            url: 'http://localhost:8080/teachers/' + teacherID
        }).then( (response) =>  {
             this.setState({teachers : response.data }, ()=> {
                 console.log(this.state);
            });
            //console.log("from axios  id = "  + response.data.teacherID + "   " + response.data.fullname + " "  + JSON.stringify(this.state));
        });
    }

    async onSubmit(event) {
        event.preventDefault();
        const newTeacher = {
            teacherId: this.refs.teacherId.value,
            fullname:this.refs.fullname.value
        }

        console.log("Test");

        await this.searchTeacher(newTeacher.teacherId);
        console.log(" teachers in submit " + this.state.teachers.length );

        // console.log(" ASTA E NR DE PROFI " + getNumber);

        if(this.state.teachers.length >= 1){
            console.log("Nu merge adaugat ca avem deja id ul asta")
            ToastsStore.error('This ID already exists in database');
        }else{
            ToastsStore.success('Teacher inserted');
            this.addTeacher(newTeacher);
        }
    }

  

    render(){
        return(
            <div>
                <form id="formm" onSubmit={this.onSubmit}>
                    <h3>Add Teacher</h3>
                        <input  class="input_register" placeholder="teacherId" ref="teacherId" id="input1"/>
                         <br />
                        <input class="input_register" placeholder="fullname" ref="fullname" id="input2"/>
                        <br/><br/>
                        <button id="butRegister">Add</button>    
                          <ToastsContainer store={ToastsStore}/>
                </form>
            </div>
        );
    }
}

const style = {margin:15};

export default TeacherRegister