import React from 'react';
import { Component } from 'react'; // {} importam o cheie
import axios from 'axios';
import { ToastsContainer, ToastsStore } from 'react-toasts';
import './activityForm.css'


class ActivityForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activities: []
    }

    this.onSubmit = this.onSubmit.bind(this); // punte de legatura intre render si onSubmit

    //handle de eveniment
    this.handleChange = (evt) => {
      this.setState({
        [evt.target.name]: evt.target.value
      })
    }
  }

  currentTime(){
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date + ' ' + time;
    return dateTime;
  }

  addActivity(activity) {
    axios.request({
      method: 'post',
      url: 'http://localhost:8080/activities',
      data: activity
    })
      .then(response => {
        this.props.history.push('/profilTeacher');
      })
      .catch(err => console.log(err));
  }


  async searchActivity(activityID) {
    await axios.request({
      method: 'GET',
      url: 'http://localhost:8080/activities/' + activityID
    }).then((response) => {
      this.setState({ activities: response.data }, () => {
        console.log("in get " + this.state.activities.length);
      });
    })
    //  }).catch(err =>
    //       ToastsStore.error("Eroare"));
  }

  async onSubmit(event) {
    event.preventDefault();
    const newActivity = {
      description: this.refs.activityDescription.value,
      keyAccess: this.refs.keyAccess.value,
      startTime: this.currentTime(),
      lengthOfTime: this.refs.duration.value,
      allowAcces:"YES"
    }
    console.log(this.currentTime());
    console.log("Test");

    await this.searchActivity(newActivity.keyAccess);
    console.log("aici testam current time:"+ newActivity.startTime);
    console.log("in submit " + this.state.activities.length)

    if (this.state.activities.length >= 1) {
      console.log("Nu merge adaugat ca avem deja id ul asta");
      ToastsStore.error('This ID already exists in database');
    } else {
      ToastsStore.success('Activity inserted');
      this.addActivity(newActivity);
    }
  }

  render() {
    return (
      <div>
        <form id="adaugareActiv" onSubmit={this.onSubmit}>
          <h3 id="heading_activity">Add Activity</h3>
          <input class="activityForm" placeholder="activityDescription" ref="activityDescription" />
          <br />
          <input class="activityForm" placeholder="keyAccess" ref="keyAccess" />
          <br />
          <input class="activityForm" placeholder="duration" ref="duration" />
          <br />
          <button id="buton_adauga_Activ">Add</button>
          <ToastsContainer store={ToastsStore} />

        </form>
      </div>
    );
  }
}

export default ActivityForm;
