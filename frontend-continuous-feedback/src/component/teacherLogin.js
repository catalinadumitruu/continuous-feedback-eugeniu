import React, { Component } from 'react'
import './teacherLogin.css'
import { ToastsContainer, ToastsStore } from 'react-toasts';
import axios from 'axios';

class TeacherLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            teachers: []
        }

        this.onSubmit = this.onSubmit.bind(this);

        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name]: evt.target.value
            })
        }
    }

    async searchTeacher(teacherID){
        await axios.request({
            method: 'GET',
            url: 'http://localhost:8080/teachers/' + teacherID
        }).then( (response) =>  {
             this.setState({teachers : response.data }, ()=> {
                 console.log(this.state);
            });
        });
    }

    async onSubmit(event) {
        event.preventDefault();
        const newTeacher = {
            teacherId: this.refs.teacherId.value,
            fullname:this.refs.fullname.value
        }

        console.log("Test");

        await this.searchTeacher(newTeacher.teacherId);
        console.log(" teachers in submit " + this.state.teachers.length );

        // console.log(" ASTA E NR DE PROFI " + getNumber);

        if(this.state.teachers.length >= 1){
            this.props.history.push('/profilTeacher');
        }else{
            ToastsStore.error('Teacher does not exist');
        }
    }


    render() {
        return (
            <div>
                <form onSubmit={this.onSubmit} id="teacherLogin">
                    <label class="label" for="Teacher Code">Teacher Code   </label>
                    <input type='text' class="input" id='teacher_code' placeholder="Enter your code" ref="teacherId" />
                    <br />
                    <br />
                    <label class="label" for="name and surname">Name and Surname Teacher      </label>
                    <input type='text' class="input" id='name_and_surname' placeholder="Enter your full name" ref="fullname"/>

                    <br />
                    <button id="buttonAddTeacher">Login</button>
                    <ToastsContainer store={ToastsStore} />
                </form>
            </div>
        );
    }
}

const style = { margin: 15 };

export default TeacherLogin