'use strict'
const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const models = require('../models');

//get all the feedbacks from Feedback table
router.get('/', async (req, res, next) =>{
   try{
        let feedback = await models.Feedback.findAll();
        res.status(200).json(feedback);
   }catch(e){
       console.warn(e);
       res.status(500).json({message:"server error in get Feedbacks"})
   }
});

//insert Feedbacks in Feedback Table
router.post('/',async (req, res, next) =>{
    try{
        if (req.query.bulk && req.query.bulk == 'on'){
            await models.Feedback.bulkCreate(req.body);
            res.status(201).json({message : 'FeedbackS created and inserted'});
        }
        else{ 
           await models.Feedback.create(req.body);
            res.status(201).json({message:"the feedback created and inserted"});
        }
    }catch(e){
            console.log(e);
            res.status(500).json({message : "error at insert the feedback"});
    }
});

//get a specific the feedback from Feedback table
// router.get('/:feedbackID', async (req, res, next) => {
//     try{
//         let feedback = await models.Feedback.findByPk(req.params.feedbackID);

//         if(feedback){
//             res.status(200).json(feedback);
//         }else{
//             res.status(404).json({message:"the feedback was not found"})
//         }
//     }catch(e){
//         console.log(e);
//         res.status(500).json({message:"error at getting a specific the feedback"})
//     }
// });

//get all the feedback for an activity
router.get('/:activityID', async (req, res, next) => {
    try{
        let feedbacks = await models.Feedback.findAll({
            where : {
                idActivity : req.params.activityID
            }
        })

        if(feedbacks){
            res.status(200).json(feedbacks);
        }else{
            res.status(404).json({message:"the feedback was not found"})
        }
    }catch(e){
        console.log(e);
        res.status(500).json({message:"error at getting a specific the feedback"})
    }
});

//get all the feedback gave by a specific student
router.get('/byStudent/:studentID', async (req, res, next) => {
    try{
        let feedbacks = await models.Feedback.findAll({
            where : {
                idStudent : req.params.studentID
            }
        })

        if(feedbacks){
            res.status(200).json(feedbacks);
        }else{
            res.status(404).json({message:"feedbacks were not found"})
        }
    }catch(e){
        console.log(e);
        res.status(500).json({message:"error at getting a student's feedback"})
    }
});


//updating a specific the feedback from Studen table
router.put('/:feedbackID',async (req, res, next) => {
   try{
        let feedback = await models.Feedback.findByPk(req.params.feedbackID);

        if(feedback){
            await feedback.update(req.body);
            res.status(200).json({message:"the feedback updated succesfully"});
        }else{
            res.status(404).json({message:"the the feedback doesn't exist yet in the datbase"});
        }
   }catch(e){
       console.log(e);
       res.status(500).json({message:"error at updating the the feedback"});
   }
});

//deleting a specific the feedback from Feedback table
router.delete('/:feedbackID',async (req, res, next) => {
    try{
        let feedback = await models.Feedback.findByPk(req.params.feedbackID);

        if(feedback){
            await feedback.destroy();
            res.status(200).json({message:"the feedback deleted succesfully"});
        }else{
            res.status(404).json({message:"the the feedback doesn't exist yet in the datbase"});
        }
   }catch(e){
       console.log(e);
       res.status(500).json({message:"error at deleting the the feedback"});
   }
});

module.exports = router;  // exported and can be used in our files
