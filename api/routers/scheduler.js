var cron = require('node-cron');
 var mysql=require('mysql2');

//vrem sa sa facem get la toate task-urile
//sa parcurgem fiecare task si sa vedem daca currentTime-timpul de start >durata daca da il stergem din baza de date  daca nu il lasam in pace

var con= mysql.createConnection({
    user : process.env.DB_USER,
    password : process.env.DB_PASSWORD,
    database:process.env.DB
});

function currentTime(){
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date + ' ' + time;
    return dateTime;
}

con.connect(function (err) {
    if(err)console.log("scheduler : Couldn t connect to database ")
    cron.schedule('*/1 * * * * *', () => {
        con.query("SELECT description, startTime,lengthOfTime,keyAccess, allowAcces FROM activities",function (err,result,fields) {
            console.log(result); 
            
            for (var i = 0; i < result.length; i++) {

                let diffOfMinutes = (Date.parse(currentTime())-Date.parse(result[i].startTime))/60000;
                // console.log(Date.parse(currentTime()));
                // console.log(new Date(Date.parse(currentTime())).toString());
                // console.log(typeof(Date.parse(currentTime())));
                // console.log(Date.parse(result[i].startTime));
                // console.log(typeof(Date.parse(result[i].startTime)));
              
                let lenOfTime = parseInt(result[i].lengthOfTime,10);
                console.log("diff of minutes " + diffOfMinutes);
                console.log("duration " + lenOfTime);
                if((diffOfMinutes > lenOfTime) && result[i].allowAcces === "YES"){
                    con.query("Update activities set allowAcces='NO' where keyAccess="+result[i].keyAccess);
                    console.log("updated " + result[i].description + " " + result[i].startTime);
                }
            }
        });
        console.log("test");
    });
    
    
});
