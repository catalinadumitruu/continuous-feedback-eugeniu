'use strict'
const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const models = require('../models');

//get all student from Student table
router.get('/', async (req, res, next) =>{
   try{
        let students = await models.Student.findAll();
        res.status(200).json(students);
   }catch(e){
       console.warn(e.stack);
       res.status(500).json({message:"server error in get students"})
   }
});

//insert students in Student Table
router.post('/',async (req, res, next) =>{
    try{
        if (req.query.bulk && req.query.bulk == 'on'){
            await models.Student.bulkCreate(req.body)
            res.status(201).json({message : 'studentS created and inserted'})
        }
        else{ 
           await models.Student.create(req.body);
            res.status(201).json({message:"student created and inserted"});
        }
    }catch(e){
            console.log(e);
            res.status(500).json({message : "error at insert student"});
    }
});

//get a specific student from Student table by id
router.get('/:studID', async (req, res, next) => {
    try{
        let stud = await models.Student.findByPk(req.params.studID);

        if(stud){
            res.status(200).json(stud);
        }else{
            res.status(404).json({message:"student was not found"})
        }
    }catch(e){
        console.log(e);
        res.status(500).json({message:"error at getting a specific student"})
    }
});

  //get a specific student from Student table by name
  router.get('/name/:studentName', async (req, res, next) => {
    try{
        let student = await models.Student.findAll({
            where :{
                surname : req.params.studentName
            }
        });

        if(student){
            res.status(200).json(student);
        }else{
            res.status(404).json({message:"student not found"})
        }
    }catch(e){
        console.log(e);
        res.status(500).json({message:"error at getting a specific student by name"})
    }
});

//updating a specific student from Studen table
router.put('/:studID',async (req, res, next) => {
   try{
        let stud = await models.Student.findByPk(req.params.studID);
        if(stud){
            await stud.update(req.body);
            res.status(200).json({message:"student updated succesfully"});
        }else{
            res.status(404).json({message:"the student doesn't exist yet in the datbase"});
        }
   }catch(e){
       console.log(e);
       res.status(500).json({message:"error at updating the student"});
   }
});

//deleting a specific student from Student table
router.delete('/:studID',async (req, res, next) => {
    try{
        let stud = await models.Student.findByPk(req.params.studID);
        if(stud){
            await stud.destroy();
            res.status(200).json({message:"student deleted succesfully"});
        }else{
            res.status(404).json({message:"the student doesn't exist yet in the datbase"});
        }
   }catch(e){
       console.log(e);
       res.status(500).json({message:"error at deleting the student"});
   }
});

module.exports = router;  // exported and can be used in our files
