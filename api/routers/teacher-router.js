const express = require('express');
const router = express.Router();

const models = require('../models');

//get all teachers from Teacher table
router.get('/', async (req, res, next) =>{
    try{
         let teachers = await models.Teacher.findAll();
         res.status(200).json(teachers);
    }catch(e){
        console.warn(e.stack);
        res.status(500).json({message:"server error in get teachers"})
    }
 });
 
 //insert teachers in Teacher Table
 router.post('/',async (req, res, next) =>{
     try{
         if (req.query.bulk && req.query.bulk == 'on'){
             await models.Teacher.bulkCreate(req.body)
             res.status(201).json({message : 'teacherS created and inserted'})
         }
         else{ 
            await models.Teacher.create(req.body);
             res.status(201).json({message:"Teacher created and inserted"});
         }
     }catch(e){
             console.log(e);
             res.status(500).json({message : "error at insert teacher"});
     }
 });
 
 //get a specific teacher from Teacher table by Id
 router.get('/:teacherId', async (req, res, next) => {
     try{
         let teacher = await models.Teacher.findAll({
            where :{
                teacherID : req.params.teacherId
            }
        });

 
         if(teacher){
             res.status(200).json(teacher);
         }else{
             res.status(404).json({message:"teacher was not found"})
         }
     }catch(e){
         console.log(e);
         res.status(500).json({message:"error at getting a specific teacher"})
     }
 });

  //get a specific teacher from Teacher table by name
  router.get('/name/:teacherName', async (req, res, next) => {
    try{
        let teacher = await models.Teacher.findAll({
            where :{
                fullname : req.params.teacherName
            }
        });

        if(teacher){
            res.status(200).json(teacher);
        }else{
            res.status(404).json({message:"teacher not found"})
        }
    }catch(e){
        console.log(e);
        res.status(500).json({message:"error at getting a specific teacher by name"})
    }
});
 
 //updating a specific teacher from Teacher table
 router.put('/:teacherID',async (req, res, next) => {
    try{
         let teacher = await models.Teacher.findByPk(req.params.teacherID);
         if(teacher){
             await teacher.update(req.body);
             res.status(200).json({message:"teacher updated succesfully"});
         }else{
             res.status(404).json({message:"the teacher doesn't exist yet in the datbase"});
         }
    }catch(e){
        console.log(e);
        res.status(500).json({message:"error at updating the teacher"});
    }
 });
 
 //deleting a specific teacher from Teacher table
 router.delete('/:teacherID',async (req, res, next) => {
     try{
         let teacher = await models.Teacher.findByPk(req.params.teacherID);
         if(teacher){
             await teacher.destroy();
             res.status(200).json({message:"teacher deleted succesfully"});
         }else{
             res.status(404).json({message:"the teacher doesn't exist yet in the datbase"});
         }
    }catch(e){
        console.log(e);
        res.status(500).json({message:"error at deleting the teacher"});
    }
 });

module.exports = router;  // exported and can be used in our files
