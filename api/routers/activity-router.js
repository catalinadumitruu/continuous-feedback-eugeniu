'use strict'
const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const models = require('../models');

//get all activities from activity table
router.get('/', async (req, res, next) =>{
   try{
        let Activity = await models.Activity.findAll();
        res.status(200).json(Activity);
   }catch(e){
       console.warn(e.stack);
       res.status(500).json({message:"server error in get Activity"})
   }
});

//insert Activity in activity Table
router.post('/',async (req, res, next) =>{
    try{
        if (req.query.bulk && req.query.bulk == 'on'){
            await models.Activity.bulkCreate(req.body)
            res.status(200).json({message : 'activityS created and inserted'})
        }
        else{ 
           await models.Activity.create(req.body);
            res.status(201).json({message:"activity created and inserted"});
        }
     }catch(e){
             console.log(e);
             res.status(500).json({message : "error at insert activity",eroare:e.message});  
     }
});

//get a specific activity from activity table
router.get('/:activityID', async (req, res, next) => {
    try{
        let activity = await models.Activity.findAll({
            where :{
                keyAccess : req.params.activityID
            }
        });
        if(activity){
            res.status(200).json(activity);
        }else{
            res.status(404).json({message:"activity was not found"})
        }
    }catch(e){
        console.log(e);
        res.status(500).json({message:"error at getting a specific activity"})
    }
});

//get the start time of a given activity
router.get('/time/:activityID', async (req, res, next) => {
    try{
        let activity = await models.Activity.findAll({
            where :{
                keyAccess : req.params.activityID
            }
        });

        if(activity){
            var time = activity[0].startTime
            res.status(200).json(time);
        }else{
            res.status(404).json({message:"activity was not found"})
        }
    }catch(e){
        console.log(e);
        res.status(500).json({message:"error at getting a specific activity"})
    }
});

//getting all the activities of a given student
// router.get('/activities/:studentID', async (req, res, next) => {
//     try{
//         let activity = await models.Activity.findAll({
//             where : {
//                 idStudent : req.params.studentID
//             }
//         })

//         if(activity){
//             res.status(200).json(activity);
//         }else{
//             res.status(404).json({message:"activities were not found"})
//         }
//     }catch(e){
//         console.log(e);
//         res.status(500).json({message:"error at getting a student's activities"})
//     }
// });

//getting all the activities of a given teacher
router.get('/activities/:teacherID', async (req, res, next) => {
    try{
        let activity = await models.Activity.findAll({
            where : {
                idTeacher : req.params.teacherID
            }
        })

        if(activity){
            res.status(200).json(activity);
        }else{
            res.status(404).json({message:"activities were not found"})
        }
    }catch(e){
        console.log(e);
        res.status(500).json({message:"error at getting a teacher's activities"})
    }
});

//getting all the feedback for the given activity id
router.get('/activities/feedback/:activityID', async (req, res, next) => {
    try{
        let feedback = await models.Feedback.findAll({
            where : {
                idActivity : req.params.activityID
            }
        })

        if(feedback){
            res.status(200).json(feedback);
        }else{
            res.status(404).json({message:"This activity has no feedback"})
        }
    }catch(e){
        console.log(e);
        res.status(500).json({message:"error at getting a activity's feedback"})
    }
});


//updating a specific activity from Activity table
router.put('/:activityID',async (req, res, next) => {
   try{
        let activity = await models.Activity.findByPk(req.params.activityID);

        if(activity){
            await activity.update(req.body);
            res.status(200).json({message:"activity updated succesfully"});
        }else{
            res.status(404).json({message:"the activity doesn't exist yet in the datbase"});
        }
   }catch(e){
       console.log(e);
       res.status(500).json({message:"error at updating the activity"});
   }
});

//deleting a specific activity from activity table
router.delete('/:activityID',async (req, res, next) => {
    try{
        let activity = await models.Activity.findByPk(req.params.activityID
        );
        if(activity){
            await activity.destroy();
            res.status(200).json({message:"activity deleted succesfully"});
        }else{
            res.status(404).json({message:"the activity doesn't exist yet in the datbase"});
        }
   }catch(e){
       console.log(e);
       res.status(500).json({message:"error at deleting the activity"});
   }
});

module.exports = router;  // exported and can be used in our files
