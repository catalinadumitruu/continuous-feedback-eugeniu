module.exports = (sequelize, DataTypes) => {
    return sequelize.define('feedback', {
        description:{
            type: DataTypes.STRING,
            allowNull:false
        },
        // emoticon:{
        //     type:DataTypes.INTEGER, //code of emoji
        //     allowNull:true
        // },
        time:{
            type: DataTypes.DATE,
            allowNull:false
        },
        // idStudent:{
        //     type: DataTypes.INTEGER,
        //     allowNull:true
        // },
        idActivity:{
            type: DataTypes.INTEGER,
            allowNull:true
        }
    });
  }
