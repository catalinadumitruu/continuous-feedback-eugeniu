module.exports = (sequelize, DataTypes) => {
    return sequelize.define('teacher', {
        teacherId:{
            type : DataTypes.INTEGER,
            allowNull: false
        },
        fullname:{
            type : DataTypes.STRING,
            allowNull: false
        }
    });
}