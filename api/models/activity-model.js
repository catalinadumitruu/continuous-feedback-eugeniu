module.exports = (sequelize, DataTypes) => {
    return sequelize.define('activity', {
        description:{
            type: DataTypes.STRING,
            allowNull:true
        },
        lengthOfTime:{     // unitate de masura : minute, pentru a verifica si la seminar
            type:DataTypes.INTEGER,
            allowNull:true
        },
        startTime:{
            type: DataTypes.STRING,
            allowNull:true
        },
        keyAccess:{
            type:DataTypes.STRING,
            allowNull:true
        },
        // idStudent:{
        //     type: DataTypes.STRING, 
        //     allowNull:true
        // },
        idTeacher:{
            type: DataTypes.STRING,
            allowNull:true
        },
        allowAcces:{
            type:DataTypes.STRING,
            allowNull:true

        }

    })
  }
