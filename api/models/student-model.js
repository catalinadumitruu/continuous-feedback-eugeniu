//modelul campurilor pentru tabela cu studenti
// unde :  sequelize reprezinta conexiunea, iar DataTypes biblioteca Sequelize
module.exports = (sequelize, DataTypes) => {
    return sequelize.define('student', {
        surname : {
            type: DataTypes.STRING,
            allowNull: false
        },
        name : {
            type : DataTypes.STRING,
            allowNull: false
        },
        year: {
            type : DataTypes.INTEGER,
            allowNull: false,
        },
        class: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    });
}