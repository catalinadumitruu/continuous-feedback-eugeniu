// definim aici new Sequelize pentru a avea acelasi context, modelele sa fie interconectate
//ceea ce inseamna ca aici definim baza de date
'use strict' 
const fs = require('fs');
const Sequelize = require('sequelize');
const mysql = require('mysql2/promise')
let conn

//creez conexiunea

mysql.createConnection({
    user : process.env.DB_USER,
    password : process.env.DB_PASSWORD
})
.then((connection) => {
    console.log("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
    conn = connection
    return connection.query('CREATE DATABASE IF NOT EXISTS ' + process.env.DB);
})
.then(() => {
    return conn.end()
})
.catch((err) => {
    console.warn(err.stack)
})

const sequelize = new Sequelize(process.env.DB, process.env.DB_USER, process.env.DB_PASSWORD, {
    dialect : 'mysql',
	define : {
		timestamps : false
	}
});


//load models ... __dirname = current folder
const db = {}; //lista cu tabele
fs.readdirSync(__dirname).forEach((file) => {
    if(file !== 'index.js'){  //pentri fiecare fisier in models, care nu e index.js
        let keyName = file.split('.')[0].split('-')[0];
        keyName = keyName[0].toUpperCase() + keyName.slice(1, keyName.length); // numele tabelei (extras in linia anterioara din numele fisierului) sa apara cu litera mare
        let moduleName = file.split('.')[0];
        console.log("*******    " + moduleName); 
        db[keyName] = sequelize.import(moduleName);
    }
});

//acum in db se afla si modelele si legaturile dintre modele: 
db.Student.hasMany(db.Feedback);
db.Student.hasMany(db.Activity);
db.Teacher.hasMany(db.Activity);
db.Activity.hasMany(db.Feedback);


// try{
//     sequelize.sync({force:true})
//     console.log(`Database & tables created here!`)
// }catch(e){
//     console.log(e);
// }

//sync all defined models to DB
sequelize.sync({force : false})
	.then(() => console.log('created'))
	.catch((error) => console.log(error))

module.exports = db;
